{-# LANGUAGE OverloadedLists #-}

module SATSolver where

import Data.Map (Map)
import qualified Data.Map as M
import Data.Set (Set)
import qualified Data.Set as S
import Data.Foldable

import CNF
import SAT

-----------------------------
-- Basic propagation
-----------------------------

propagate :: (Show v, Eq v) => v -> Bool -> CNF v -> CNF v
propagate _ _ [] = []
propagate v b (c : cs)
  | if b then appearsPos v c else appearsNeg v c =
      propagate v b cs
  | if b then appearsNeg v c else appearsPos v c =
      filter (\at -> at /= Pos v && at /= Neg v) c : propagate v b cs
  | otherwise = c : propagate v b cs
  where appearsPos v c = Pos v `elem` c
        appearsNeg v c = Neg v `elem` c

-----------------------------
-- Naive backtracking
-----------------------------

naive :: (Show v, Ord v) => (CNF v -> v) -> CNF v -> Sat v Bool
naive pick cnf =
  case trivial cnf of
    Just sat -> sat
    Nothing ->
      case naive pick (propagate v True cnf) of
        Sat assig -> Sat (M.insert v True assig)
        Unsat ->
          case naive pick (propagate v False cnf) of
            Sat assig -> Sat (M.insert v False assig)
            Unsat -> Unsat
      where v = pick cnf

-----------------------------
-- DPLL
-----------------------------

unitClause :: CNF v -> Maybe (v, Bool)
unitClause ([v] : cnf) =
  case v of
    Pos v -> Just (v, True)
    Neg v -> Just (v, False)
unitClause (_ : cnf) = unitClause cnf
unitClause [] = Nothing

unitPropagate :: (Show v, Ord v) => CNF v -> (CNF v, Map v Bool)
unitPropagate cnf =
  case unitClause cnf of
    Nothing -> (cnf, [])
    Just (v, b) -> (pcnf, M.insert v b assig)
      where (pcnf, assig) = unitPropagate (propagate v b cnf)

data PureLits v = PureLits { positives :: Set v
                           , negatives :: Set v
                           }

pureLitterals :: (Show v, Ord v) => CNF v -> PureLits v
pureLitterals cnf = lits
  where (lits, _) =
          foldr foldDisj (PureLits [] [], []) cnf

        foldDisj disj (lits, impures) =
          foldr (\atom (lits@(PureLits pos negs), impures) ->
                    case atom of
                      Pos v | v `S.notMember` impures ->
                              if v `S.member` negs
                              then (PureLits pos (S.delete v negs), S.insert v impures)
                              else (PureLits (S.insert v pos) negs, impures)
                      Neg v | v `S.notMember` impures ->
                              if v `S.member` pos
                              then (PureLits (S.delete v pos) negs, S.insert v impures)
                              else (PureLits pos (S.insert v negs), impures)
                      _ -> (lits, impures)) (lits, impures) disj

assignPureLitterals :: (Show v, Ord v) => CNF v -> (CNF v, Map v Bool)
assignPureLitterals = go []
  where go assig cnf
          | S.null pos && S.null neg = (cnf, assig)
          | otherwise =
            go (assig <> newAssig)
               (foldr (\v cnf -> propagate v True cnf)
                      (foldr (\v cnf -> propagate v False cnf) cnf neg) pos)
          where PureLits pos neg = pureLitterals cnf
                newAssig = M.fromSet (const True) pos <> M.fromSet (const False) neg

trivial :: CNF v -> Maybe (Sat v Bool)
trivial [] = Just (Sat M.empty)
trivial cnf | any null cnf = Just Unsat
trivial _ = Nothing

dpll :: (Show v, Ord v) => (CNF v -> v) -> CNF v -> Sat v Bool
dpll pick cnf =
  case trivial cnf of
    Just sat -> sat
    Nothing ->
      case trivial scnf of
        Just Unsat -> Unsat
        Just (Sat assig) -> Sat (unitPropAssig <> pureAssigs <> assig)
        Nothing ->
          case dpll pick (propagate v True scnf) of
            Sat assig -> Sat (M.insert v True (unitPropAssig <> pureAssigs <> assig))
            Unsat ->
              case dpll pick (propagate v False scnf) of
                Sat assig -> Sat (M.insert v False (unitPropAssig <> pureAssigs <> assig))
                Unsat -> Unsat
          where v = pick scnf
      where (pcnf, unitPropAssig) = unitPropagate cnf
            (scnf, pureAssigs) = assignPureLitterals pcnf

trivialPick :: Ord v => CNF v -> v
trivialPick = S.findMin . freeVars

-----------------------------
-- Sat def
-----------------------------

anySat :: (Show v, Ord v) => CNF v -> Sat v Bool
anySat = dpll trivialPick

assignmentCNF :: Ord v => Map v Bool -> CNF v
assignmentCNF =
  M.foldrWithKey (\k b comp -> (if b then var k else negVar k) .&& comp) true

allSat :: (Show v, Ord v) => CNF v -> [Map v Bool]
allSat cnf =
  case anySat cnf of
    Unsat -> []
    Sat assig ->
      assig : allSat (cnf .&& neg (assignmentCNF assig))

-----------------------------
-- Example formulae
-----------------------------

ex1 :: CNF Name
ex1 =
  [ [Neg "a", Pos "b", Pos "c"]
  , [Pos "a", Pos "c", Pos "d"]
  , [Pos "a", Pos "c", Neg "d"]
  , [Pos "a", Neg "c", Pos "d"]
  , [Pos "a", Neg "c", Neg "d"]
  , [Neg "b", Neg "c", Pos "d"]
  , [Neg "a", Pos "b", Neg "c"]
  , [Neg "a", Neg "b", Pos "c"]
  ]

ex2 :: CNF Name
ex2 =
  [ [Neg "a", Pos "b", Pos "c"]
  , [Pos "a", Pos "c", Pos "d"]
  , [Pos "a", Pos "c", Neg "d"]
  , [Pos "a", Neg "c", Pos "d"]
  , [Pos "a", Neg "c", Neg "d"]
  , [Neg "b", Neg "c", Pos "d"]
  , [Neg "a", Pos "b", Neg "c"]
  , [Neg "a", Neg "b", Pos "c"]
  , [Neg "a", Neg "b", Neg "c", Neg "d"]
  ]

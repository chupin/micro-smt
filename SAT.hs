{-# LANGUAGE OverloadedLists #-}

module SAT where

import Data.Map (Map)
import qualified Data.Map as M
import Data.Set (Set)
import qualified Data.Set as S
import Data.Foldable

import CNF

-----------------------------
-- Sat result
-----------------------------

data Sat v k = Sat (Map v k)
             | Unsat

instance (Show v, Show k) => Show (Sat v k) where
  show Unsat = "unsat"
  show (Sat assig) =
    unlines ("sat" :
             M.foldrWithKey (\v k ls -> ("  " ++ show v ++ " : " ++ show k) : ls) [] assig)

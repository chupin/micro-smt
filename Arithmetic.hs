{-# LANGUAGE OverloadedLists #-}

module Arithmetic where

import           Data.Map            (Map)
import qualified Data.Map            as M
import qualified Data.Map.Merge.Lazy as M
import           Data.Set            (Set)
import qualified Data.Set            as S

import           CNF                 (Name)

-- Represents an linear arithmetic expression as a dictionnary from a name to
-- the factor associated with it. A constant is associated with the Nothing
-- name. Great care is taken when constructing it that no factor is 0.
--
-- Example:
--
-- 3 * x + 2 * y + 7 is represented as:
--
-- > M.fromList [(Just "x", 3), (Just "y", 2), (Nothing, 7)]
newtype Expr = Expr (Map (Maybe Name) Integer)

-- Divides an expression by the GCD of its coefficients
reduce :: Expr -> Expr
reduce (Expr expr)
  | length expr <= 1 = Expr expr
  | otherwise = Expr (fmap (`div` g) expr)
  where g = foldr1 gcd expr

eval :: Map Name Integer -> Expr -> Integer
eval env (Expr expr) =
  M.foldrWithKey (\k f acc ->
                    case k of
                      Nothing -> f + acc
                      Just n  -> f * (env M.! n) + acc) 0 expr

instance Show Expr where
  show (Expr expr) = go (M.toList expr)
    where showFact (Nothing, n) = show n
          showFact (Just v, n)  = show n ++ " * " ++ v

          go []       = ""
          go [t]      = showFact t
          go (t : ts) = showFact t ++ " + " ++ go ts

coeff :: Expr -> Maybe Name -> Integer
coeff (Expr term) name = M.findWithDefault 0 name term

coefficients :: Expr -> Set Name
coefficients (Expr expr) =
  M.foldrWithKey (\k _ vs ->
                     case k of
                       Just n  -> S.insert n vs
                       Nothing -> vs) [] expr

constant :: Integer -> Expr
constant 0 = Expr M.empty
constant n = Expr (M.singleton Nothing n)

var :: Integer -> Name -> Expr
var 0 _    = Expr M.empty
var n name = Expr (M.singleton (Just name) n)

infixl 7  .*
infixl 6  .+, .-

(.+) :: Expr -> Expr -> Expr
Expr p .+ Expr q =
  Expr $ M.merge M.preserveMissing
                 M.preserveMissing
                 (M.zipWithMaybeMatched add)
                 p q
  where add _ p q
          | n == 0 = Nothing
          | otherwise = Just n
          where n = p + q

(.-) :: Expr -> Expr -> Expr
p .- Expr q =
  p .+ Expr (fmap negate q)

(.*) :: Integer -> Expr -> Expr
k .* Expr p = Expr (fmap (k *) p)

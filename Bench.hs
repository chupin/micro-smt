module Main where

import CNF             (parser)
import SATSolver

import Criterion.Main
import Criterion.Types

names = [ "uf20-0" ++ show num ++ ".cnf"
        | num <- [1..10]
        ] -- ++
        -- [ "uuf250-0" ++ show num ++ ".cnf"
        -- | num <- [1..100]
        -- ]

main =
  defaultMainWith
  (defaultConfig { reportFile = Just "bench/report.html" })
    [ bgroup ("UUF 250-" ++ kindName)
      [ env (readFile ("bench/" ++ name)) $ \file ->
          bench name (whnf (kind . parser) file)
      | name <- names
      ]
    | (kind, kindName) <-
        [ (anySat, "DPLL")
        , (naive trivialPick, "Naive")
        ]
    ]

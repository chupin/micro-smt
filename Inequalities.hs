{-# LANGUAGE OverloadedLists #-}

module Inequalities where

import Data.Set (Set)
import qualified Data.Set as S
import Data.Map (Map)
import qualified Data.Map as M
import qualified Data.Map.Merge.Lazy as M
import Data.List (partition)
import Data.IntegerInterval
import qualified Data.IntegerInterval as I
import Data.Maybe (fromJust)
import Data.Foldable (toList)
import Control.Monad

import SAT
import CNF hiding (freeVars, var, neg)
import Arithmetic

data Cond = Sub0 | SubEq0

instance Show Cond where
  show Sub0 = "< 0"
  show SubEq0 = "≤ 0"

data Pred = Pred Cond Expr

instance Show Pred where
  show (Pred cd expr) = show expr ++ " " ++ show cd

freeVars :: Pred -> Set Name
freeVars (Pred _ expr) = coefficients expr

trivial :: Pred -> Bool
trivial (Pred cd expr) =
  case cd of
    Sub0 -> val < 0
    SubEq0 -> val <= 0
  where val = coeff expr Nothing

fourier :: ([Pred] -> Name)
        -> [Pred]
        -> Maybe (Name, [Pred], [Pred])
fourier pick terms
  | S.null (foldMap freeVars terms) = Nothing
  | otherwise = Just (v, fmap reducePred bds, fmap reducePred rem)
  where v = pick terms
        (bds, rem) = elim v terms

        reducePred (Pred cd expr) = Pred cd (reduce expr)

elim :: Name -> [Pred] -> ([Pred], [Pred])
elim v terms =
  (lowerBounds ++ upperBounds, newEqns ++ notHaveV)
  where (haveV, notHaveV) = partition (\term -> v `elem` freeVars term) terms

        factors =
          fmap (\(Pred _ term) -> abs (coeff term (Just v))) haveV

        mkMult xs (y : ys) = product (xs ++ ys) : mkMult (y : xs) ys
        mkMult _ [] = []

        scale fact (Pred cd expr) = Pred cd (fact .* expr)

        scaledHaveV = zipWith scale (mkMult [] factors) haveV

        leadingFact = product factors

        leadingVar = var leadingFact v

        (lowerBounds, upperBounds) =
          foldr (\pred@(Pred cd expr) (lbs, ubs) ->
                   if coeff expr (Just v) < 0
                   then (lbs, pred : ubs)
                   else (pred : lbs, ubs)) ([], []) scaledHaveV

        newEqns = [ Pred (case (cd1, cd2) of
                            (Sub0, _) -> Sub0
                            (_, Sub0) -> Sub0
                            (SubEq0, SubEq0) -> SubEq0) (upper .+ lower)
                  | Pred cd1 upper <- upperBounds
                  , Pred cd2 lower <- lowerBounds
                  ]

elimAll :: [Pred] -> [(Name, [Pred])]
elimAll preds =
  snd $ foldr (\v (xs, bs) -> let (b, ys) = elim v xs in
                                (b ++ ys, (v, b) : bs)) (preds, []) vs
  where vs = foldMap freeVars preds

solve1 :: Map Name IntegerInterval
       -> Name
       -> [Pred]
       -> IntegerInterval
solve1 ivals v preds = vival
  where vival = foldr (\pred vivals -> solve pred `intersection` vivals) whole preds
        solve (Pred cd expr@(Expr exprDict)) = ival
          where (r, m) = (-q) `divMod` p

                ival
                  | p < 0 = Finite bound <=..< PosInf
                  | otherwise = NegInf <..<= Finite bound

                bound =
                  case (cd, m) of
                    (Sub0, 0) -> r - 1
                    _ -> r

                p = coeff expr (Just v)
                q = sum $ M.mapWithKey foldConst exprDict

                foldConst Nothing factor = factor
                foldConst (Just u) factor
                  | u == v = 0
                  | factor > 0 =
                      case M.lookup u ivals of
                        Nothing -> error "impossible"
                        Just ubounds ->
                          factor * case upperBound ubounds of
                                     Finite up -> up
                                     _ -> fromJust $ pickup ubounds
                  | otherwise =
                      case M.lookup u ivals of
                        Nothing -> error "impossible"
                        Just ubounds ->
                          factor * case lowerBound ubounds of
                                     Finite low -> low
                                     _ -> fromJust $ pickup ubounds

unknown :: Map Name k -> Set Name -> Set Name
unknown known =
  S.filter (`S.notMember` M.keysSet known)

solve :: ([Pred] -> Name)
      -> [Pred]
      -> Maybe (Map Name Integer)
solve pick terms = fmap (fmap (fromJust . pickup)) assigs
  where assigs = go terms

        go terms =
          case fourier pick terms of
            Nothing | all trivial terms -> Just []
                    | otherwise -> Nothing
            Just (v, bdv, newPreds) ->
              case go newPreds of
                Nothing -> Nothing
                Just assig -> solveBounds assig bdv

solveBounds assig bounds =
  case partition (\(_, unknowns) -> length unknowns <= 1) unknownVars of
    ([], []) -> Just assig
    ([], _) ->
      solveBounds (M.insert mv (I.singleton 0) assig) bounds
      where mv = S.findMin $ snd $ head $ unknownVars
    (simplBounds@(_:_), otherBounds) -> do
      let foldSimpl preds (pred, vars) =
            case toList vars of
              []
                | trivial pred -> Just preds
                | otherwise -> Nothing
              [v] -> Just (M.insertWith (++) v [pred] preds)

      simplPreds <- foldM foldSimpl [] simplBounds
      assig <- M.foldrWithKey (\v preds assig -> do
                                  assig <- assig
                                  let val = solve1 assig v preds
                                  when (I.null val) Nothing
                                  Just (M.insert v val assig)) (Just assig) simplPreds
      solveBounds assig (fmap fst otherBounds)
  where unknownVars =
          fmap (\pred -> (pred, unknown assig (freeVars pred))) bounds

neg :: Pred -> Pred
neg (Pred SubEq0 expr) = Pred Sub0 (constant 0 .- expr)
neg (Pred Sub0 expr) = Pred SubEq0 (constant 0 .- expr)

decide :: [(Pred, Bool)] -> Sat Name Integer
decide sat =
  case solve trivialPick (fmap (\(p, b) -> if b then p else neg p) sat) of
    Just assig -> Sat assig
    Nothing -> Unsat

elimUnderConstraint :: [Pred] -> Maybe (Set Name, Set Name)
elimUnderConstraint preds
  | S.null pos && S.null neg = Nothing
  | otherwise = Just (pos, neg)
  where (pos, neg) =
          ( S.filter (\v -> v `S.notMember` ubs) lbs
          , S.filter (\v -> v `S.notMember` lbs) ubs
          )
        (lbs, ubs, all) =
          foldr (\pred@(Pred _ expr) (lbs, ubs, all) ->
                   foldr (\v (lbs, ubs, all) ->
                            if coeff expr (Just v) > 0
                            then (S.insert v lbs, ubs, S.insert v all)
                            else (lbs, S.insert v ubs, S.insert v all))
                  (lbs, ubs, all) (coefficients expr)) ([], [], []) preds

trivialPick :: [Pred] -> Name
trivialPick  = S.findMin . foldMap freeVars

--           foldr (\pred@(Pred cd expr) (lbs, ubs) ->
--                    if coeff expr (Just v) < 0
--                    then (lbs, pred : ubs)
--                    else (pred : lbs, ubs)) ([], []) scaledHaveV
ex1 :: [Pred]
ex1 = [ Pred SubEq0 (var 2 "x" .+ constant 4 .+ var (-1) "y")
      , Pred Sub0 (var 1 "y" .+ constant (-9))
      ]

ex2 :: [Pred]
ex2 = [ Pred Sub0 (var 2 "x" .- var 1 "y")
      , Pred SubEq0 (var 3 "y")
      ]

ex3 :: [Pred]
ex3 = [ Pred SubEq0 (  var (-1) "x"
                      .+ var 5 "y"
                      .+ var (-2) "z"
                      .+ constant 7
                    )
      , Pred SubEq0 (  var (-3) "x"
                      .+ var 2 "y"
                      .+ var 6 "z"
                      .+ constant (-12)
                    )
      , Pred SubEq0 (  var 2 "x"
                      .+ var (-5) "y"
                      .+ var 4 "z"
                      .+ constant (-10)
                    )
      , Pred SubEq0 (  var 3 "x"
                      .+ var (-6) "y"
                      .+ var 3 "z"
                      .+ constant (-9)
                    )
      , Pred SubEq0 (  var 10 "y"
                      .+ var (-1) "z"
                      .+ constant (-15)
                    )
      ]

ex4 :: [Pred]
ex4 = [ Pred SubEq0 ( var (-1) "x" .+ var 1 "y" .- constant 1
                    )
      , Pred SubEq0 ( var 3 "x" .+ var 2 "y" .- constant 12
                    )
      , Pred SubEq0 ( var 2 "x" .+ var 3 "y" .- constant 12
                    )
      , Pred SubEq0 ( var (-1) "x" )
      , Pred SubEq0 ( var (-1) "y" )
      ]

-- data Ival = Ival (Maybe Integer) (Maybe Integer)

-- open :: Ival
-- open = Ival Nothing Nothing

-- boundUp :: Integer -> Ival -> Maybe Ival
-- boundUp nup (Ival low up)
--   | nup < low = Nothing
--   | otherwise = Just (Ival low (min nup up))

-- boundDown :: Integer -> Ival -> Maybe Ival
-- boundDown nlow (Ival low up)
--   | nlow > up = Nothing
--   | otherwise = Just (Ival (max low nlow) up)

{-# LANGUAGE OverloadedLists, FlexibleContexts #-}

module SMT where

import Data.Set (Set)
import qualified Data.Set as S
import Control.Monad.State
import qualified Data.Map as M

import CNF
import SATSolver
import SAT
import qualified Inequalities as Arith

import Data.Map (Map)

data Tagged k = Tagged Int k
              deriving(Show)

untag :: Tagged k -> k
untag (Tagged _ k) = k

instance Eq (Tagged k) where
  Tagged n _ == Tagged m _ = n == m

instance Ord (Tagged k) where
  Tagged n _ `compare` Tagged m _ = n `compare` m

toBoolCNF :: CNF k -> CNF (Tagged k)
toBoolCNF cnf = tcnf
  where (tcnf, (_, _)) = runState (traverse (traverse tag) cnf) (0, [])

        tag (Pos k) = do
          idx <- fresh
          let tagged = Tagged idx k
          modify (\(idx, tags) -> (idx, S.insert tagged tags))
          pure (Pos tagged)

        tag (Neg k) = do
          idx <- fresh
          let tagged = Tagged idx k
          modify (\(idx, tags) -> (idx, S.insert tagged tags))
          pure (Neg tagged)

        fresh = do
          (idx, tags) <- get
          put (idx + 1, tags)
          pure idx

smt :: Show k => ([(k, Bool)] -> Sat u v) -> CNF k -> Sat u v
smt decide cnf = smt' (toBoolCNF cnf)
  where smt' cnf =
          case anySat cnf of
            Unsat -> Unsat
            Sat assig ->
              case decide (fmap (\(tg, b) -> (untag tg, b)) (M.toList assig)) of
                Unsat -> smt' (cnf .&& neg (assignmentCNF assig))
                Sat sat -> Sat sat

smtArith :: CNF Arith.Pred -> Sat Name Integer
smtArith = smt Arith.decide

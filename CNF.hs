{-# LANGUAGE OverloadedLists #-}

module CNF where

import           Data.Map (Map)
import qualified Data.Map as M
import           Data.Set (Set)
import qualified Data.Set as S
import Data.Maybe

-----------------------------
-- CNF definition
-----------------------------

type Name = String

data Atom v = Pos v
            | Neg v
            deriving(Eq)

instance Show v => Show (Atom v) where
  show (Pos n) = show n
  show (Neg n) = '!' : show n

evalAtom :: Ord v
         => Map v Bool
         -> Atom v
         -> Bool
evalAtom env (Pos v) = env M.! v
evalAtom env (Neg v) = not (env M.! v)

type CNF v = [[Atom v]]

eval :: Ord v => Map v Bool -> CNF v -> Bool
eval env = and . fmap (any (evalAtom env))

freeVars :: Ord v => CNF v -> Set v
freeVars = foldr (\atom fvs ->
                    case atom of
                      Pos v -> S.insert v fvs
                      Neg v -> S.insert v fvs) [] . concat

-----------------------------
-- CNF construction
-----------------------------

true :: CNF v
true = []

false :: CNF v
false = [[]]

var :: v -> CNF v
var v = [[Pos v]]

negVar :: v -> CNF v
negVar v = [[Neg v]]

neg :: CNF v -> CNF v
neg cnf = sequence negsDnf
  where negsDnf = fmap (fmap negAtom) cnf

        negAtom (Pos v) = Neg v
        negAtom (Neg v) = Pos v

(.&&) :: CNF v -> CNF v -> CNF v
(.&&) = (++)

(.||) :: CNF v -> CNF v -> CNF v
p .|| q = neg (neg p .&& neg q)

parser :: String -> CNF Name
parser =
  go .
  takeWhile (\c -> head c /= '%') .
  dropWhile (\c -> head c == 'p' || head c == 'c') .
  lines
  where go =
          fmap (mapMaybe (\w -> let n = read w in
                             case n `compare` 0 of
                               EQ -> Nothing
                               LT -> Just (Neg (show (abs n)))
                               GT -> Just (Pos (show n))) . words)
